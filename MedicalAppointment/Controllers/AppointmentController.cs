﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/Appoitment")]
    public class AppointmentController : ApiController
    {
        LoginContext DB = new LoginContext();
        [Route("Addappoitment")]
        [HttpPost]
        public object AddotrUpdateappointment(Appointment st)
        {
            try
            {
                if (st.Id == 0)
                {
                    Appointment sm = new Appointment();
                    sm.Confrimdate = st.Confrimdate;
                    sm.Doctortype = st.Doctortype;
                    sm.Problemdescription = st.Problemdescription;
                    sm.Counseledbefore = st.Counseledbefore;
                    sm.UserMail = st.UserMail;
                    DB.Appointments.Add(sm);
                    DB.SaveChanges();
                    return new Response
                    {
                        Status = "Success",
                        Message = "Data Successfully"
                    };
                }
                else
                {
                    var obj = DB.Appointments.Where(x => x.Id == st.Id).ToList().FirstOrDefault();
                    if (obj.Id > 0)
                    {
                        obj.Confrimdate = st.Confrimdate;
                        obj.Doctortype = st.Doctortype;
                        obj.Problemdescription = st.Problemdescription;
                        obj.Counseledbefore = st.Counseledbefore;
                        obj.UserMail = st.UserMail;
                        DB.SaveChanges();
                        return new Response
                        {
                            Status = "Updated",
                            Message = "Updated Successfully"
                        };
                    }
                }
               
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return new Response
            {
                Status = "Error",
                Message = "Data not insert"
            };

        }
        [Route("Appointmentdetails")]
        [HttpGet]
        public object Appointmentdetails()
        {

            var a = DB.Appointments.ToList();
            return a;
        }
        [Route("AppointmentById")]
        [HttpGet]
        public object AppointmentById(int id)
        {
            var obj = DB.Appointments.Where(x => x.Id == id).ToList().FirstOrDefault();
            return obj;
        }
        [Route("Deleteappointment")]
        [HttpDelete]
        public object Deletestudent(int id)
        {
            var obj = DB.Appointments.Where(x => x.Id == id).ToList().FirstOrDefault();
            DB.Appointments.Remove(obj);
            DB.SaveChanges();
            return new Response
            {
                Status = "Delete",
                Message = "Delete Successfuly"
            };
        }
    }
}

