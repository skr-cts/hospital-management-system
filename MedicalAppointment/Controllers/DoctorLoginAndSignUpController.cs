﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/login")]
    public class DoctorLoginandSignupController : ApiController
    {
        private LoginContext DB = new LoginContext();
        [Route("InsertDoctor")]
        [HttpPost]
        public object InsertDoctor(DoctorRegistration Reg)
        {
            try
            {
                DoctorRegistration PL = new DoctorRegistration();
                if (PL.DoctorId == 0)
                {
                    PL.Name = Reg.Name;
                    PL.UserName = Reg.UserName;
                    PL.HighestDegree = Reg.HighestDegree;
                    PL.Age = Reg.Age;
                    PL.Speciality = Reg.Speciality;
                    PL.Email = Reg.Email;
                    PL.Password = Reg.Password;
                    DB.DoctorRegistrations.Add(PL);
                    DB.SaveChanges();
                    return new Response
                    { Status = "Success", Message = "Record SuccessFully Saved." };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        //Login
        [Route("DoctorLogin")]
        [HttpPost]
        public Response DoctorLogin(DoctorLogin login)
        {
            var log = DB.DoctorRegistrations.Where(x => x.Email.Equals(login.Email) &&
                      x.Password.Equals(login.Password)).FirstOrDefault();

            if (log != null)
            {
                return new Response { Status = "Success", Message = "Login Successfully." };
            }
            return null;
        }
    }
}