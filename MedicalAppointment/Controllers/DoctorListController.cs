﻿using System;
using System.Linq;
using System.Web.Http;
using MedicalAppointment.Models;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/Doctor")]
    public class DoctorListController : ApiController
    {
        LoginContext DB = new LoginContext();
        [Route("AddDoctor")]
        [HttpPost]
        public object AddDoctor(DoctorList st)
        {
            try
            {
                if (st.Id == 0)
                {
                    DoctorList sm = new DoctorList();
                    sm.DoctorName = st.DoctorName;
                    sm.DoctorSpecilaity = st.DoctorSpecilaity;
                    sm.DoctorAvilability = st.DoctorAvilability;
                    DB.DoctorLists.Add(sm);
                    DB.SaveChanges();
                    return new Response
                    {
                        Status = "Success",
                        Message = "Data Successfully"
                    };
                }
                else
                {
                    var obj = DB.DoctorLists.Where(x => x.Id == st.Id).ToList().FirstOrDefault();
                    if (obj.Id > 0)
                    {
                        obj.DoctorName = st.DoctorName;
                        obj.DoctorSpecilaity = st.DoctorSpecilaity;
                        obj.DoctorAvilability = st.DoctorAvilability;
                        DB.SaveChanges();
                        return new Response
                        {
                            Status = "Updated",
                            Message = "Updated Successfully"
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return new Response
            {
                Status = "Error",
                Message = "Data not insert"
            };

        }
        [Route("DoctorDetails")]
        [HttpGet]
        public object DoctorDetails()
        {

            var a = DB.DoctorLists.ToList();
            return a;
        }
        [Route("DoctorListById")]
        [HttpGet]
        public object DoctorListById(int id)
        {
            var obj = DB.DoctorLists.Where(x => x.Id == id).ToList().FirstOrDefault();
            return obj;
        }
        [Route("DeleteDoctor")]
        [HttpDelete]
        public object DeleteDoctor(int id)
        {
            var obj = DB.DoctorLists.Where(x => x.Id == id).ToList().FirstOrDefault();
            DB.DoctorLists.Remove(obj);
            DB.SaveChanges();
            return new Response
            {
                Status = "Delete",
                Message = "Delete Successfuly"
            };
        }

    }
}
