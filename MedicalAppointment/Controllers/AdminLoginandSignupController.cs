﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;
namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/Admin")]
    public class AdminLoginandSignupController : ApiController
    {
        private LoginContext DB = new LoginContext();
        [Route("AdminSignup")]
        [HttpPost]
        public object InsertAdmin(AdminLoginandSignup Reg)
        {
            try
            {
                AdminLoginandSignup PL = new AdminLoginandSignup();
                if (PL.Id == 0)
                {

                    PL.Email = Reg.Email;
                    PL.Password = Reg.Password;
                    DB.AdminLoginandSignups.Add(PL);
                    DB.SaveChanges();
                    return new Response
                    { Status = "Success", Message = "Admin Signup SuccessFully Saved." };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        //Login
        [Route("AdminLogin")]
        [HttpPost]
        public Response AdminLogin(PatientLogin login)
        {
            var log = DB.AdminLoginandSignups.Where(x => x.Email.Equals(login.Email) &&
                      x.Password.Equals(login.Password)).FirstOrDefault();

            if (log != null)
            {
                return new Response { Status = "Success", Message = "Admin Login Successfully." };
            }
            return null;
        }
    }
}
