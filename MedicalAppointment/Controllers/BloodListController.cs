﻿using MedicalAppointment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/Blood")]
    public class BloodListController : ApiController
    {
        LoginContext DB = new LoginContext();
        [Route("AddBlood")]
        [HttpPost]
        public object AddBlood(BloodList vt)
        {
            try
            {
                if (vt.Id == 0)
                {
                    BloodList vm = new BloodList();
                    //vm.BloodGroup = vt.BloodGroup;
                    vm.BloodGroup = vt.BloodGroup;
                    vm.BloodAvilability = vt.BloodAvilability;
                    BloodList bloodList = DB.BloodLists.Add(vm);
                    DB.SaveChanges();
                    return new Response
                    {
                        Status = "Success",
                        Message = "Data Successfully"
                    };
                }
                else
                {
                    var obj = DB.BloodLists.Where(x => x.Id == vt.Id).ToList().FirstOrDefault();
                    if (obj.Id > 0)
                    {
                        //obj.BloodGroup = vt.BloodGroup;
                        obj.BloodGroup = vt.BloodGroup;
                        obj.BloodAvilability = vt.BloodAvilability;
                        DB.SaveChanges();
                        return new Response
                        {
                            Status = "Updated",
                            Message = "Updated Successfully"
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return new Response
            {
                Status = "Error",
                Message = "Data not insert"
            };

        }
        [Route("BloodDetails")]
        [HttpGet]
        public object BloodDetails()
        {

            var a = DB.BloodLists.ToList();
            return a;
        }
        [Route("BloodListById")]
        [HttpGet]
        public object BloodListById(int id)
        {
            var obj = DB.BloodLists.Where(x => x.Id == id).ToList().FirstOrDefault();
            return obj;
        }
        [Route("DeleteBlood")]
        [HttpDelete]
        public object DeleteBlood(int id)
        {
            var obj = DB.BloodLists.Where(x => x.Id == id).ToList().FirstOrDefault();
            DB.BloodLists.Remove(obj);
            DB.SaveChanges();
            return new Response
            {
                Status = "Delete",
                Message = "Delete Successfuly"
            };
        }
    }
}
