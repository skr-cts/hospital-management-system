﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/VaccineList")]
    public class VaccineListController : ApiController
    {
        LoginContext DB = new LoginContext();
        [Route("AddLimit")]
        [HttpPost]
        public object AddLimit(VaccineList st)
        {
            try
            {
                if (st.Id == 0)
                {
                    VaccineList sm = new VaccineList();
                    sm.AvailVaccineName = st.AvailVaccineName;
                    sm.Limit = st.Limit;
                    DB.VaccineLists.Add(sm);
                    DB.SaveChanges();
                    return new Response
                    {
                        Status = "Success",
                        Message = "Data Successfully"
                    };
                }
                else
                {
                    var obj = DB.VaccineLists.Where(x => x.Id == st.Id).ToList().FirstOrDefault();
                    if (obj.Id > 0)
                    {
                        obj.AvailVaccineName = st.AvailVaccineName;
                        obj.Limit = st.Limit;
                        DB.SaveChanges();
                        return new Response
                        {
                            Status = "Updated",
                            Message = "Updated Successfully"
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return new Response
            {
                Status = "Error",
                Message = "Data not insert"
            };

        }
        [Route("VaccineList")]
        [HttpGet]
        public object VaccineList()
        {

            var a = DB.VaccineLists.ToList();
            return a;
        }

        //[Route("DoctorListById")]
        //[HttpGet]
        //public object DoctorListById(int id)
        //{
        //    var obj = DB.DoctorLists.Where(x => x.Id == id).ToList().FirstOrDefault();
        //    return obj;
        //}
        
        [Route("DeleteVaccine")]
        [HttpDelete]
        public object DeleteVaccine(int id)
        {
            var obj = DB.VaccineLists.Where(x => x.Id == id).ToList().FirstOrDefault();
            DB.VaccineLists.Remove(obj);
            DB.SaveChanges();
            return new Response
            {
                Status = "Delete",
                Message = "Delete Successfuly"
            };
        }

    }
}
