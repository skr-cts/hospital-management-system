﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;


namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/Vaccine")]

    public class VaccinationController : ApiController
    {
        private LoginContext DB = new LoginContext();
        [Route("VaccinationRegistration")]
        [HttpPost]
        public object VaccinationRegistration(Vaccination st)
        {
            try
            {
                if (st.Id == 0)
                {
                    Vaccination sm = new Vaccination();

                    sm.ConfrimDateOfVaccination = st.ConfrimDateOfVaccination;
                    sm.VaccineName = st.VaccineName;
                    sm.ChooseSlot = st.ChooseSlot;
                    sm.IdProof = st.IdProof;
                    sm.UserMail = st.UserMail;
                    DB.Vaccinations.Add(sm);
                    DB.SaveChanges();
                    return new Response
                    {
                        Status = "Success",
                        Message = "Data Successfully"
                    };
                }
                else
                {
                    var obj = DB.Vaccinations.Where(x => x.Id == st.Id).ToList().FirstOrDefault();
                    if (obj.Id > 0)
                    {
                        obj.ConfrimDateOfVaccination = st.ConfrimDateOfVaccination;
                        obj.VaccineName = st.VaccineName;
                        obj.ChooseSlot = st.ChooseSlot;
                        obj.IdProof = st.IdProof;
                        obj.UserMail = st.UserMail;
                        DB.SaveChanges();
                        return new Response
                        {
                            Status = "Updated",
                            Message = "Updated Successfully"
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return new Response
            {
                Status = "Error",
                Message = "Data not insert"
            };

        }
        [Route("VaccinationDeatils")]
        [HttpGet]
        public object VaccinationDeatils()
        {

            var a = DB.Vaccinations.ToList();
            return a;
        }
        [Route("VaccinationById")]
        [HttpGet]
        public object VaccinationById(int id)
        {
            var obj = DB.Vaccinations.Where(x => x.Id == id).ToList().FirstOrDefault();
            return obj;
        }
        [Route("DeleteVaccination")]
        [HttpDelete]
        public object DeleteVaccination(int id)
        {
            var obj = DB.Vaccinations.Where(x => x.Id == id).ToList().FirstOrDefault();
            DB.Vaccinations.Remove(obj);
            DB.SaveChanges();
            return new Response
            {
                Status = "Delete",
                Message = "Delete Successfuly"
            };
        }

    }
    }


