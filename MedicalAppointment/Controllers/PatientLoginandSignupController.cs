﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedicalAppointment.Models;

namespace MedicalAppointment.Controllers
{
    [RoutePrefix("Api/login")]
    public class PatientLoginandSignupController : ApiController
    {
        private LoginContext DB = new LoginContext();
        [Route("InsertPatient")]
        [HttpPost]
        public object InsertPatient(PatientRegistration Reg)
        {
            try
            {
                PatientRegistration PL = new PatientRegistration();
                if (PL.PatientId == 0)
                {
                    PL.FirstName = Reg.FirstName;
                    PL.LastName = Reg.LastName;
                    PL.Email = Reg.Email;
                    PL.Mobile = Reg.Mobile;
                    PL.Gender = Reg.Gender;
                    PL.Age = Reg.Age;
                    PL.Password = Reg.Password;
                    DB.PatientRegistrations.Add(PL);
                    DB.SaveChanges();
                    var name = PL.FirstName + " " + PL.LastName;
                    return new Response
                    { Status = "Success", Message = PL.Email, Name = name};
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        //Login
        [Route("PatientLogin")]
        [HttpPost]
        public Response PatientLogin(PatientLogin login)
        {

            var log = DB.PatientRegistrations.Where(x => x.Email.Equals(login.Email) &&
                      x.Password.Equals(login.Password)).FirstOrDefault();
            var getName = log.FirstName + " " + log.LastName;

            if (log != null)
            {
                return new Response { Status = "Success", Message = login.Email, Name = getName};
            }
            return null;
    }
}
}