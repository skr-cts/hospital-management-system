﻿namespace MedicalAppointment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminLoginandSignups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Confrimdate = c.String(),
                        Doctortype = c.String(),
                        Problemdescription = c.String(),
                        Counseledbefore = c.String(),
                        UserMail = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BloodLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BloodGroup = c.String(),
                        BloodAvilability = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DoctorLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorName = c.String(),
                        DoctorSpecilaity = c.String(),
                        DoctorAvilability = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DoctorRegistrations",
                c => new
                    {
                        DoctorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserName = c.String(),
                        HighestDegree = c.String(),
                        Age = c.String(),
                        Speciality = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.DoctorId);
            
            CreateTable(
                "dbo.PatientRegistrations",
                c => new
                    {
                        PatientId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Mobile = c.String(),
                        Gender = c.String(),
                        Age = c.Int(nullable: false),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.PatientId);
            
            CreateTable(
                "dbo.Vaccinations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConfrimDateOfVaccination = c.String(),
                        VaccineName = c.String(),
                        ChooseSlot = c.String(),
                        IdProof = c.String(),
                        UserMail = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VaccineLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AvailVaccineName = c.String(),
                        Limit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VaccineLists");
            DropTable("dbo.Vaccinations");
            DropTable("dbo.PatientRegistrations");
            DropTable("dbo.DoctorRegistrations");
            DropTable("dbo.DoctorLists");
            DropTable("dbo.BloodLists");
            DropTable("dbo.Appointments");
            DropTable("dbo.AdminLoginandSignups");
        }
    }
}
