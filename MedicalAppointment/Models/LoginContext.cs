﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MedicalAppointment.Models;
namespace MedicalAppointment.Models
{
    public class LoginContext:DbContext
    {
        public LoginContext() : base("Connection")
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<StudentContext, DemoWebApiEF.Migrations.Configuration>("Connection"));
        }
        public DbSet<PatientRegistration> PatientRegistrations { get; set; }
        public DbSet<DoctorRegistration> DoctorRegistrations { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AdminLoginandSignup> AdminLoginandSignups { get; set; }

        public DbSet<Vaccination> Vaccinations { get; set; }

        public DbSet<DoctorList> DoctorLists { get; set; }
        
        public DbSet<VaccineList> VaccineLists { get; set; }
        public DbSet<BloodList> BloodLists { get; set; }
    }
}