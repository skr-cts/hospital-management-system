﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedicalAppointment.Models
{
    public class PatientLogin
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}